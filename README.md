## Codename: sudhub - StartUpDorf

// todo: Add a description

First, we need access to the twitter api. Following https://apps.twitter.com/, create a new application to obtain the twitter API credentials.

In the next step, we have to create a folder 'private' and add a file that is named twitter.js.

```shell
mkdir private
vi private/twitter.json
``` 

Then, add the following code with your twitter API credentials:

```json
{
    "consumer_key": "",
    "consumer_secret": "",
    "access_token": "",
    "access_token_secret": ""
}
```

Run meteor and go to localhost:3000 with your browser.
