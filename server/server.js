// https://twitter.com/StartupDorf/lists/startups-im-startupdorf

var Twit = Meteor.npmRequire('twit');
var T = new Twit(JSON.parse(Assets.getText("twitter.json")));

var callback = function (callback) {
    console.log("cb" + callback)
};

// Determine list members
// todo: check if a member has been removed
Meteor.setInterval(function () {
    checkMembers();
}, 1000 * 60 * 15);

function checkMembers() {
    T.get('lists/members', {
        id: 1282843634,
        list_id: 95809626,
        count: 5000
    }, Meteor.bindEnvironment(function (err, data, response) {
        if (err) {

        } else {
            data.users.forEach(function (user) {
                Members.update({twitter_id_str: user.id_str}, {twitter_id_str: user.id_str}, {upsert: true})
            });
        }
    }));
}

checkMembers();

// Retrieve latest tweets
// https://dev.twitter.com/rest/reference/get/lists/statuses
T.get('lists/statuses', {
    list_id: 95809626,
    owner_id: 1282843634,
    count: 5000
}, Meteor.bindEnvironment(function (err, data, response) {
    data.forEach(function (tweet) {
        Posts.update(
            {
                id: tweet.id
            },
            {
                user: {
                    name: tweet.user.name,
                    screen_name: tweet.user.screen_name,
                    location: tweet.user.location
                },
                text: tweet.text,
                created_at: new Date(tweet.created_at),
                id: tweet.id,
                id_str: tweet.id_str
            },
            {upsert: true});
    });
}));

// Begin to stream
Meteor.setTimeout(function () {
    // MongoDB destinct workaround
    var follow = _.uniq(Members.find({}, {
        sort: {twitter_id_str: 1}, fields: {twitter_id_str: true}
    }).fetch().map(function (x) {
        return x.twitter_id_str;
    }), true);

    var stream = T.stream('statuses/filter', {follow: follow});

    stream.on('tweet', Meteor.bindEnvironment(function (data) {
        Posts.insert({
            user: {
                name: data.user.name,
                screen_name: data.user.screen_name,
                location: data.user.location,
                id_str: data.user.id_str
            },
            text: data.text,
            created_at: new Date(data.created_at),
            id: data.id,
            id_str: data.id_str
        });
    }))
}, 10000);